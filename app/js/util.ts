export default class Util {
	// https://stackoverflow.com/questions/5767325/how-do-i-remove-a-particular-element-from-an-array-in-javascript
	static removeFromArray(element: any, array: Array<any>) {
		var index = array.indexOf(element);
		if (index > -1) {
			array.splice(index, 1);
		}
		return array;
	}
	// Source: https://stackoverflow.com/questions/295566/sanitize-rewrite-html-on-the-client-side
	static sanitize(input: string) {
		var tagBody = '(?:[^"\'>]|"[^"]*"|\'[^\']*\')*';
		var tagOrComment = new RegExp(
			'<(?:'
			// Comment body.
			+ '!--(?:(?:-*[^->])*--+|-?)'
			// Special "raw text" elements whose content should be elided.
			+ '|script\\b' + tagBody + '>[\\s\\S]*?</script\\s*'
			+ '|style\\b' + tagBody + '>[\\s\\S]*?</style\\s*'
			// Regular name
			+ '|/?[a-z]'
			+ tagBody
			+ ')>',
			'gi');
		var oldHtml : string;
		do {
			oldHtml = input;
			input = input.replace(tagOrComment, '');
		} while (input !== oldHtml);
		return input.replace(/</g, '&lt;');
	}
}